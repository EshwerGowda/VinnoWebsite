/**
 * Created by akshay on 27/6/17.
 */

const Q = require('q');
const apiService = require('../APIServices/sendgridAPIService');

const sgService = {};


sgService.subscribe = function (userDetails, host) {
    const deferred = Q.defer();
  apiService.subscribe(userDetails, getRegistrationLink(userDetails, host)).then(function (response) {
    deferred.resolve(response);
  }, function (error) {
    deferred.reject(error);
  });
  return deferred.promise;
};

sgService.addToContacts = function (userDetails) {
    const deferred = Q.defer();
  apiService.addToContacts(userDetails).then(function (response) {
    deferred.resolve(response);
  }, function (error) {
    deferred.reject(error);
  });
  return deferred.promise;
};


function getRegistrationLink(userDetails, host) {
    let baseUrl = 'http://' + host + '/api/addToContacts?email=' + userDetails.email;
  /*TODO : Uncomment the below code once the prod server moved to https*/
 /* let baseUrl = '';
  if (host.includes('localhost')) {
    baseUrl = 'http://' + host + '/api/addToContacts?email=' + userDetails.email;
  } else {
    baseUrl = 'https://' + host + '/api/addToContacts?email=' + userDetails.email;
  }
*/
  //Validate User fields
  if (userDetails.firstName) baseUrl = baseUrl + '&firstName=' + userDetails.firstName;
  if (userDetails.lastName) baseUrl = baseUrl + '&lastName=' + userDetails.lastName;
  if (userDetails.mobileNumber) baseUrl = baseUrl + '&mobileNumber=' + userDetails.mobileNumber;

  return baseUrl;
}
module.exports = sgService;
