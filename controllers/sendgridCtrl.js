/**
 * Created by akshay on 27/6/17.
 */

const express = require('express');
const router = express.Router();
const path = require('path');
const service = require('./sendgridService');

// routes
router.post('/subscribe', subscribe);
router.get('/addToContacts', addToContacts);

module.exports = router;

function subscribe(req, res) {
  service.subscribe(req.body, req.headers.host).then(function (response) {
    res.status(200).send(response);
  }, function (error) {
    res.status(401).send(response);
  });
}

function addToContacts(req, res) {
  service.addToContacts(req.query).then(function (response) {
    res.redirect(getHomePageUrl(req.headers.host) + '/#!/thank-you');
  }, function (error) {
    res.redirect(getHomePageUrl(req.headers.host) + '/#!/unsuccessful');
  })
}

function getHomePageUrl(host) {
  let url = 'http://' + host;
  /*TODO : Uncomment the below code once the prod server moved to https*/
  /*let url = '';
  if (host.includes('localhost')) {
    url = 'http://' + host;
  } else {
    url = 'https://' + host;
  }*/
  return url;
}
