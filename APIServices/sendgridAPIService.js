/**
 * Created by akshay on 27/6/17.
 */

const Q = require('q');
const fs = require('fs');
const helper = require('sendgrid').mail;
const config = require('../../sendgrid.config.json');
const sg = require('sendgrid')(config.SEND_GRID_API_KEY);
const fromEmail = new helper.Email(config.FROM_EMAIL, config.FROM_USERNAME);

const subject = 'subject';
const content = new helper.Content(
    'text/html', '<h1>Content</h1>');

const apiService = {};

apiService.subscribe = function (userDetails, registerList) {
    const deferred = Q.defer();

    const userName = userDetails.firstName + ' ' + userDetails.lastName;
    const toEmail = new helper.Email(userDetails.email);

    const mail = new helper.Mail(fromEmail, subject, toEmail, content);


    //Add substitutions
    mail.personalizations[0].addSubstitution(
        new helper.Substitution('-name-', userName));
    mail.personalizations[0].addSubstitution(
        new helper.Substitution('-firstName-', userDetails.firstName));
    mail.personalizations[0].addSubstitution(
        new helper.Substitution('-link-', registerList));

    //Add Transactional template id
    mail.setTemplateId(config.SUBSCRIBE_TEMPLATE_ID);

    const request = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: mail.toJSON()
    });

    sg.API(request, function (error, response) {
        if (error) {
            deferred.reject(error);
        }
        deferred.resolve(response);
    });
    return deferred.promise;
};


apiService.addToContacts = function (userDetails) {
    const deferred = Q.defer();

    const user = {};
    if (userDetails.email) user.email = userDetails.email;
    if (userDetails.firstName) user.first_name = userDetails.firstName;
    if (userDetails.lastName) user.last_name = userDetails.lastName;
    if (userDetails.mobileNumber) user.mobile_number = userDetails.mobileNumber;

    const requestBody = [user];

    const request = sg.emptyRequest({
        method: 'POST',
        path: '/v3/contactdb/recipients',
        body: requestBody
    });

    sg.API(request, function (error, response) {
        if (error) {
            deferred.reject(error);
        }
        addContactToList(response.body.persisted_recipients[0]).then(function (response) {
            sendThankYouEmail(user.email).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
        }, function (error) {
            deferred.reject(error);
        })
    });

    return deferred.promise;
};

function sendThankYouEmail(emailId) {
    const deferred = Q.defer();

    const toEmail = new helper.Email(emailId);
    const mail = new helper.Mail(fromEmail, subject, toEmail, content);

    //Add Transactional template id
    mail.setTemplateId(config.THANK_YOU_TEMPLATE_ID);

    const request = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: mail.toJSON()
    });

    sg.API(request, function (error, response) {
        if (error) {
            deferred.reject(error);
        }
        deferred.resolve(response);
    });
    return deferred.promise;
}

function addContactToList(contactId) {
    const deferred = Q.defer();

    getDefaultListId().then(function (listId) {

        const request = sg.emptyRequest({
            method: 'POST',
            path: '/v3/contactdb/lists/' + listId + '/recipients/' + contactId
        });

        sg.API(request, function (error, response) {
            if (error) {
                deferred.reject(error);
            }
            deferred.resolve(response);
        });
    }, function (error) {
        deferred.reject(error);
    });
    return deferred.promise;
}

function addDefaultList() {
    const deferred = Q.defer();

    const request = sg.emptyRequest({
        method: 'POST',
        path: '/v3/contactdb/lists',
        body: {
            name: config.LIST_NAME
        }
    });

    sg.API(request, function (error, response) {
        if (error) {
            deferred.reject(error);
        }
        updateListId(response.body.id);
        deferred.resolve(response);
    });

    return deferred.promise;
}

function getDefaultListId() {
    const deferred = Q.defer();

    const request = sg.emptyRequest({
        method: 'GET',
        path: '/v3/contactdb/lists'
    });

    sg.API(request, function (error, response) {
        if (error) {
            deferred.reject(error);
        }
        let listId = -1;
        if (response.body && response.body.lists && response.body.lists.length) {
            response.body.lists.forEach(function (list, index) {
                if (list.name === config.LIST_NAME) {
                    listId = list.id;
                }
            });
        }
        if (listId !== -1) {
            deferred.resolve(listId);
        } else {
            deferred.reject({message: 'listNotFound'})
        }
    });

    return deferred.promise;
}

function ensureDefaultListAdded() {
    const deferred = Q.defer();

    getDefaultListId().then(function (listId) {
        updateListId(listId);
        deferred.resolve(listId);
    }, function (error) {
        addDefaultList();
    });

    return deferred.promise;
}

function updateListId(listId) {
    fs.readFile('sendgrid.config.json', function (err, data) {
        const json = JSON.parse(data);
        json.LIST_ID = listId;
        fs.writeFile("sendgrid.config.json", JSON.stringify(json))
    });
}

ensureDefaultListAdded();

module.exports = apiService;
