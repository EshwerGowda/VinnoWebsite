![Webpack Angular ES6](http://geniuscarrier.me/images/webpack-angular-es6.png)
# Webpack Angular ES6
A boilerplate for writing modular Angular 1.X in ES6 using Webpack.

## Quick start

### Install dependencies
```
npm install
```
#### Live
```
npm run live
```
In your browser, navigate to: http://localhost:8080/
#### Test
```
npm run test
```

#### Production with NodeJS server

```
npm start
```
